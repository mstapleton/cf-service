## CF Service Documentation


### Running the project locally
System Requirements
```
- PHP 7.4+
- git
- composer
- npm
```

In a terminal window, clone the project (Linux or macOS):
```
git clone https://mstapleton@bitbucket.org/mstapleton/cf-service.git
cd cf-service
composer install
```

Create the sqlite3 file to use as the local database:
```
touch database/database.sqlite
```

Create the .env file:
```
cp .env.example .env
```

Edit the .env file

- Delete the following lines:
```
DB_CONNECTION=mysql
DB_HOST=127.0.0.1
DB_PORT=3306
DB_DATABASE=laravel
DB_USERNAME=root
DB_PASSWORD=
```
- Replace with:
```
DB_CONNECTION=sqlite
DB_HOST=127.0.0.1
DB_PORT=3306
DB_DATABASE=/path/to/cf-service/database/database.sqlite #The absolute path to the db file
```

Run:
```
php artisan migrate
php artisan key:generate
php artisan serve
```
In a new/separate terminal window, from the same cf-service directory, run:
```
npm install
npm run watch
```

The project should now be accessible via local IP and port.

Verify the site loads in the local web browser:
```
http://127.0.0.1:8000
```


### Endpoints

Web Browser
- GET `/` - visit to see a Vue datatable of all the requests

APIs
- GET `/difference?n={number}`
  - Description:
    - Calculates the 
  - Param:
    - number (integer between 1-100)
  - Response (JSON)
    - 
```
      {
          "datetime": "YYYY-MM-DD HH:mm:ss",
          "value": ,
          "number": {n},
          "occurrences": {number of times requested}
      }
```

- GET `/logs`
    - Description:
      - Get a log of all requests made to the `/difference` endpoint


The Postman collection with example API requests:
```
CF Service.postman_collection.json
```
