<?php

namespace App\Http\Controllers;

use App\Models\Log;
use App\Helpers\CalculationHelper;
use Illuminate\Http\Request;

class DifferenceController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return array
     */
    public function index(Request $request)
    {
        $n = (int)$request->query('n');

        $difference = (new CalculationHelper())->difference($n);

        $log = new Log;
        $log->number = $n;
        $log->result = $difference;
        $log->save();

        $occurrence = Log::where('number', '=', $n)->count();

        return [
                'datetime' => date("Y-m-d H:i:s"),
                'value' => $difference,
                'number' => $n,
                'occurrences' => $occurrence
        ];
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
