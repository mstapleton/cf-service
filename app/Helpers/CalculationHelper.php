<?php

namespace App\Helpers;

class CalculationHelper {

    private function sumOfTheSquaresToN(int $n): int
    {
        $sum = 0;
        for ($i = 1; $i <= $n; $i++) {
            $sum += $i * $i;
        }

        return $sum;
    }

    private function squareOfTheSumToN(int $n): int
    {
        $sum = 0;
        for ($i = 1; $i <= $n; $i++) {
            $sum += $i;
        }

        return $sum * $sum;
    }

    public function difference(int $n): int
    {
        // if invalid value of n, default to 0
        if (!isset($n) || !$n || $n <= 0 || $n > 100) {
            $n = 0;
        }

        return $this->squareOfTheSumToN($n) - $this->sumOfTheSquaresToN($n);
    }
}
