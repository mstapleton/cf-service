<?php

namespace Tests\Feature;

use App\Helpers\CalculationHelper;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class CalculationHelperTest extends TestCase
{
    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function test_example()
    {
        $response = $this->get('/');
        $response->assertStatus(200);
    }

    public function testDifferenceOutOfRangeMinHandled()
    {
        $n = -1000;
        $difference = (new CalculationHelper())->difference($n);
        $this->assertEquals(0, $difference);
    }

    public function testDifferenceOutOfRangeMaxHandled()
    {
        $n = 1000;
        $difference = (new CalculationHelper())->difference($n);
        $this->assertEquals(0, $difference);
    }

    public function testDifferenceValueAccuracy()
    {
        $n = 10;
        $difference = (new CalculationHelper())->difference($n);
        $this->assertEquals(2640, $difference);
    }

}
